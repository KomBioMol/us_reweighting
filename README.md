# Brief description
This script allows to reweight a free energy profile to a free energy profile along another reaction coordinate. The values of current coordinate and the coordinate of interest have 
to be provided as input data. The script was written with python 2.7 - please take that into consideration while using it.
# Input data
One should provide a file with names of data files, the center of the added potential, force constant in kcal/mol and F parameters obtained from wham procedure. An example of an input
is provided as "example_input.txt". 
Data files should provide two columns: with values of the current reaction coordinate and with the values of new reaction coordinate. 
# How to run the script
Here is an example command to run the script:
python reweighting_simple.py -f input_example.txt -o output_example.txt -T temperature -b number_of_bins
where temperature refers to the temperature at which the free energy profile was computed and -b defines a number of bins in the output file (name of the file stated after -o).
IMPORTANT
Now there is no limit to the size of the weight. Thus the script can be numerically unstable for the points that are far away from the center of the potential. 
The general recommendation for the usage is as follows: try to remove all of the points that deviate more than e.g. 0.6 Angstrom from the center of the potential. Do the reweighting
and then repeat for larger cutoff, e.g. 0.7 Angstrom. At some point you will see that the distribution deviates strongly from previous distributions - this is the cutoff at which
the weight values become numerically unstable. Finally, use the largest cutoff for which none of the unexpected deviations (like large probability peaks) were observed.
