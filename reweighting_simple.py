import argparse
# import mdtraj as md
# from msmbuilder.cluster import KMedoids
import numpy as np
# import matplotlib.pyplot as plt
# from ndxparser import NDX
# from desfunc import Descriptors
"""
This script reweights the free energy profile to another, selected coordinate.
One should be aware that the script does not have any limitations for the size of the weight for now.
Thus, some artificial "peaks" of the probability might occur."
"""



kB = 0.001987 # Boltzmann's constant [kcal/molK]

def PARSER():
    parser = argparse.ArgumentParser(description='Reading the input, temperature and the number of expected bins')
    parser.add_argument('-f', '--inp',     metavar='N', type=str,   nargs='+', help = 'input file')
    parser.add_argument('-o', '--out', metavar='file.out', type=str, nargs=1, help='output name')
    parser.add_argument('-T', '--temp',    metavar='r', type=float, nargs=1,   help = 'temperature')
    parser.add_argument('-b', '--bins', metavar='s', type=float, nargs=1, help='number of bins')
    args = parser.parse_args()
    return args

def parse_input(input):
    return np.loadtxt(input, dtype={'names': ('filename', 'd', 'k', 'f'), 'formats': ('S50', 'f4', 'f4', 'f4')})


def create_weights(rc, d, k, f, T):
    weights = np.exp(-(f - 0.5*k*np.power((rc - d),2))/(kB*T))
    return weights # / np.sum(weights)

def main():

    input = parse_input(PARSER().inp[0])
    T = PARSER().temp[0]
    bin_nums=int(PARSER().bins[0])
    print(bin_nums)
    weights = None #np.array([])
    new_rc = None
    for i in range(len(input['filename'])):
        coords=np.array(np.loadtxt(input[i]['filename']))
        rc=coords[:,0]
        n_rc=coords[:,1]
        weight = create_weights(rc, input[i]['d'], input[i]['k'], input[i]['f'], T)
        if type(weights) != type(None):
            weights = np.concatenate((weights, weight))
        else:
            weights = weight
        if type(new_rc) != type(None):
            new_rc = np.concatenate((new_rc, n_rc))
        else:
            new_rc = n_rc

    result = np.histogram(new_rc, weights=weights, bins=bin_nums)
    bin_size=result[1][1]-result[1][0]
    x_axis=[]
    for nbins in range(bin_nums):
        x_axis.append(result[1][nbins]+bin_size/2)
    y_axis = result[0]
    to_list = np.vstack((x_axis,y_axis))
    to_list=to_list.T
    np.savetxt(PARSER().out[0], to_list)
if __name__ == '__main__':
    from datetime import datetime
    startTime = datetime.now()
    main()
    print(datetime.now() - startTime)
    import time
    time.sleep(1)
